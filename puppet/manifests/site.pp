Exec
{
  path => ['/usr/bin', '/bin', '/usr/sbin', '/sbin', '/usr/local/bin', '/usr/local/sbin']
}

exec
{
    'apt-get update':
        command => '/usr/bin/apt-get update'
}


include other
include user::virtual
include user::sysadmins
include user::developers
include mysql
include php
class { 'nginx': target => "/vagrant/public", }
include php5fpm
include beanstalk
include composer

