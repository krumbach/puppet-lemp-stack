class nginx($target)
{
  package { "nginx":
      ensure => present,
      require => Exec ["apt-get update"]
  }
    
  service { "nginx":
    require => Package["nginx"],
    ensure => running,
    enable => true
  }

  file { "/var/www":
        ensure  => "link",
        target  => $target,
        require => Package["nginx"],
        notify  => Service["nginx"],
        force => true
  }
  
  file { "/etc/nginx/sites-enabled/default":
      require => Package["nginx"],
      ensure  => absent,
      notify  => Service["nginx"]
  }
  
  file { "/etc/nginx/sites-available/demo1":
      require => Package["nginx"],
      ensure => "file",
      source => "puppet:///modules/nginx/host.conf",
      notify => Service["nginx"]
  }
  
  
  file { "/etc/nginx/sites-enabled/demo1":
      require => File["/etc/nginx/sites-available/demo1"],
      ensure => "link",
      target => "/etc/nginx/sites-available/demo1",
      notify => Service["nginx"]
  }
}