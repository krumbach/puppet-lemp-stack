class other 
{
    package { "curl":
            ensure  => present,
            require => Exec['apt-get update']
    }
    
    package { "augeas-tools":
            ensure  => present,
            require => Exec['apt-get update']
    }
}
