class user::virtual {
	@user { 'tkrumbach': ensure => present, managehome => true, }	
	@user { 'rcravens': ensure => present, managehome => true, }
	@user { 'TomoQaSystem': ensure => present, managehome => true,}
}

class user::developers {
	realize(User['tkrumbach'])
	realize(User['rcravens'])
}

class user::sysadmins {
	realize(User['tkrumbach'])
	realize(User['rcravens'])
	realize(User['TomoQaSystem'])	
}

