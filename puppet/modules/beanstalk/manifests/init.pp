class beanstalk {
 
    package { "beanstalkd":
            ensure  => present,
            require => Exec['apt-get update']
    }

    service { "beanstalkd":
            enable => true,
            ensure => running,
            require => Package["beanstalkd"],
    }
}