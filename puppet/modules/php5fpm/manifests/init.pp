class php5fpm 
{
  package { 'php5-fpm':
      ensure => present,
      require => Exec ["apt-get update"]
  }
  
  service { 'php5-fpm':
      ensure => running,
      enable => true,
      subscribe => File['php.ini','www.conf'],
  }
  
  file { 'www.conf':
      path => '/etc/php5/fpm/pool.d/www.conf',
      ensure => file,
      require => Package['php5-fpm'],
  }
  
  file { 'php.ini':
      path => '/etc/php5/fpm/php.ini',
      ensure => file,
      require => Package['php5-fpm'],
  }
  
  augeas { 'www.conf':
    require => Package['php5-fpm'],
    notify => Service["php5-fpm"],
    context => "/files/etc/php5/fpm/pool.d/www.conf",
    incl => '/etc/php5/fpm/pool.d/www.conf', 
    changes => ['set /files/etc/php5/fpm/pool.d/www.conf/www/listen /var/run/php5-fpm.sock'],
    lens => 'PHP.lns', 
  }
  
  augeas { 'php.ini':
    require => Package['php5-fpm'],
    notify => Service["php5-fpm"],
    context => "/files/etc/php5/fpm/php.ini",
    incl => '/etc/php5/fpm/php.ini', 
    changes => ['set /files/etc/php5/fpm/php.ini/cgi/cgi.fix_pathinfo 0'],
    lens => 'PHP.lns',
  }
}

