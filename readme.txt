sudo apt-get install git
git clone https://krumbach@bitbucket.org/krumbach/puppet-lemp-stack.git

chmod 700 boostrap.sh
sudo ./bootstrap.sh

edit web path in manifests/site.pp

cd puppet
sudo puppet apply --modulepath ./modules manifests/site.pp

sudo passwd <username>
sudo usermod -aG sudo <username>
groups <username>


# mysql
/sbin/iptables -A INPUT -i eth0 -p tcp --destination-port 3306 -j ACCEPT

SELECT User FROM mysql.user;

CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
grant all on [database_name].* to [user]@[ip_address] identified by '[password]'
grant all on mydb.* to tkrumbach@'%' identified by '[password]'
GRANT ALL PRIVILEGES ON *.* TO tkrumbach@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;

mysql -u [username]-p
mysql -h10.11.14.54 -u -p

************rabbit mq****************

sudo apt-get remove rabbitmq-server
sudo apt-get install python-software-properties
sudo add-apt-repository "deb http://www.rabbitmq.com/debian/ testing main"
wget http://www.rabbitmq.com/rabbitmq-signing-key-public.asc
sudo apt-key add rabbitmq-signing-key-public.asc
sudo apt-get update
sudo apt-get install rabbitmq-server -y
sudo service rabbitmq-server start
sudo rabbitmq-plugins enable rabbitmq_management
sudo service rabbitmq-server restart
sudo apt-get install php5-dev
cd /tmp/
wget https://github.com/alanxz/rabbitmq-c/releases/download/v0.5.0/rabbitmq-c-0.5.0.tar.gz    (URL from: https://github.com/alanxz/rabbitmq-c)
tar -zxvf rabbitmq-c-0.5.0.tar.gz
cd rabbitmq-c-0.5.0/
./configure
sudo apt-get install make
make
sudo make install
cd ..
wget http://pecl.php.net/get/amqp-1.2.0.tgz    (URL: http://pecl.php.net/package/amqp)
tar -zxvf amqp-1.2.0.tgz 
cd amqp-1.2.0/
phpize && ./configure --with-amqp && make && sudo make install
sudo nano /etc/php5/cli/php.ini 

inside nano
ctrl-w , .so, enter
extension = amqp.so
ctrl-x, Y, enter

cd /vagrant/public/
sudo service php5-fpm restart
sudo service nginx restart