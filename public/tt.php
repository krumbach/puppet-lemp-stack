<?php
$username = "guest"; // default user
$password = "guest"; // default password
$virtualhost = "/";
$exchange_name = "amq.fanout";
$routing_key = "hello_world";
$queue_name = "hello_world_queue";
$message = "Hello World!";

// Connecting to Rabbit MQ

$amqpConnection = new AMQPConnection();
$amqpConnection->setLogin($username);
$amqpConnection->setPassword($password);
$amqpConnection->setVhost($virtualhost);
$amqpConnection->connect();

if (!$amqpConnection->isConnected()) {
  die("Cannot connect to the broker, exiting !".PHP_EOL);
}

// Sending message to queue

$channel = new AMQPChannel($amqpConnection);
$exchange = new AMQPExchange($channel);
$exchange->setName($exchange_name);
$exchange->setType("fanout");

// Setting up the queue

$queue = new AMQPQueue($channel);
$queue->setName($queue_name);
$queue->declare();
$queue->bind($exchange_name, $routing_key);

$result = $exchange->publish($message, $routing_key);
if (!$message) {
  echo "Error: Message '" . $result . "' was not sent.",PHP_EOL;
} else {
  echo "Message '" . $result . "' sent.",PHP_EOL;
}

sleep(1);

// Getting message from queue

$msg = $queue->get();
echo $msg->getBody(),PHP_EOL;

// Removing message from queue

$queue->ack($msg->getDeliveryTag());

// Removing queue

$queue->delete();

if (!$amqpConnection->disconnect()) {
  die("Could not disconnect!".PHP_EOL);
}

?>
